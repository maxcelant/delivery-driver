using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Delivery : MonoBehaviour
{

    // used to change the color of the vehicle
    [SerializeField] Color32 hasPackageColor = new Color32(35, 190, 54, 255);
    [SerializeField] Color32 noPackageColor = new Color32(255, 255, 255, 255);
    bool hasPackage = false; // tells if you have the package or not
    [SerializeField] float destroyDelay = 0.5f;

    // initialize SpriteRenderer
    SpriteRenderer spriteRenderer; 


    void Start() 
    {
        spriteRenderer = GetComponent<SpriteRenderer>(); // gets the Sprite Renderer component and stores it in variable 
    }

    void OnTriggerEnter2D(Collider2D other) 
    {
        // if the object you go through is of tag "Package" then, execute the if statement
        if(other.tag == "Package" && !hasPackage) 
        {
            Debug.Log("Picked up!");
            hasPackage = true;

            spriteRenderer.color = hasPackageColor;

            Destroy(other.gameObject, destroyDelay); // destroys the package
        }
        // if the object you go through is of tag "Customer" then, execute the if statement
        if(other.tag == "Customer" && hasPackage)
        {
            Debug.Log("Package Delivered!");
            hasPackage = false;

            spriteRenderer.color = noPackageColor; // changes the color back to normal

        }
        else if(other.tag == "Customer" && hasPackage == false)
        {
            Debug.Log("You don't have my package!! :( ");
        }
        
    }

}
