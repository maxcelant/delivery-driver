using System;
using System.Threading;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Driver : MonoBehaviour
{
    [SerializeField] float steerSpeed = 325.0f; // angle of turning
    [SerializeField] float moveSpeed = 10.0f; // speed of car
    [SerializeField] float slowSpeed = 6.0f; // speed when slowed
    [SerializeField] float boostSpeed = 20.0f; // speed when boosted
    [SerializeField] float temp = 10.0f;
    [SerializeField] float speedTime = 6.0f;


    void Start()
    {
        
    }

    void Update()
    {
        // WASD input from player
        float steerAmount = Input.GetAxis("Horizontal") * steerSpeed * Time.deltaTime; // gets the X-direction input
        float speedAmount = Input.GetAxis("Vertical") * moveSpeed * Time.deltaTime;

        // steering speed and car speed
        transform.Rotate(0, 0, -steerAmount); 
        transform.Translate(0, speedAmount, 0);
    }

    // works as a timer, it waits 'speedTime' seconds before going back to regular speed
    IEnumerator SpeedValue()
    {
        yield return new WaitForSeconds(speedTime);
        moveSpeed = temp; 
    }

    void OnCollisionEnter2D(Collision2D other) 
    {
        // if you hit into something, it will slow you down
        moveSpeed = slowSpeed;
        StartCoroutine(SpeedValue());   
    }

    void OnTriggerEnter2D(Collider2D other) 
    {
        // if you run into something tagged Booster, you'll get sped up
        if (other.tag == "Booster")
        {
            moveSpeed = boostSpeed;
            StartCoroutine(SpeedValue());
        }
    }
}

